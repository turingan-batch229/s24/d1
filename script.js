// console.log("Hello World!");

/*
	ES6 Updates

	ES6 is one of the latest versions of writing Javascript and in fact is one of the latest major update to JS.

	let,const - are ES6 updates to update the  standard of creating variables.
	var -  was the keyword used previously before ES6

*/

// console.log(sampleLet);
// let sampleLet = "Sample";

// console.log(varSample);
// var varSample = "Hoist Me Up!";

// Exponent Operator

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
// Math.pow() allows us to get the result of a number raised to the given exponent
// Math.pow(base,exponent);

// Exponent Operators - ** - allows us to get the result of a number raised to a given exponent. It is used as an alternative to Math.pow()

let fivePowerOf2 = 5**2;
console.log(fivePowerOf2);//25

// Can we also get the square root of a number with exponent operator?
let squareRootOf4 = 4**.5;
console.log(squareRootOf4);//2
 
// Template Literals
// "", '' - string literals

// How do we combine strings?
let word1 = "Javascript";
let word2 = "Java";
let word3 = "is";
let word4 = "not";

// let sentence1 = word1 + " " + word3 + " " + word4 + " " + word2 + ".";
// console.log(sentence1);

// `` - backticks - Template Literals - allows us to create strings using `` and easily embed JS expressions.

// ${} is ised in template literals to embed JS expressions and variables.
// ${} - placeholder 
let sentence1 = `${word1} ${word3} ${word4} ${word2}.`
console.log(sentence1);
let sentence2 = `${word2} is an OOP Language.`
console.log(sentence2);

// Template Literals can also be used to embed JS expressions
let sentence3 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence3);

let user1 = {
	name: "Michael",
	position: "Manager",
	income: 90000,
	expenses: 50000
}

console.log(`${user1.name} is a ${user1.position}`);
console.log(`His income is ${user1.income} and expenses at ${user1.expenses}. His current balance is ${user1.income - user1.expenses}`);

// Destructuring Arrays and Objects
// Destructuringa will allow us to save Array elements or Object properties into new variables without having to create/initialize with accessing items/properties one by one.

let array1 = ["Curry","Lilliard","Paul","Irving"];

/*let player1 = array1[0];//Curry
let player2 = array1[1];//Lilliard
let player3 = array1[2];//Paul
let player4 = array1[3];//Irving

console.log(player1,player2,player3,player4);
*/

// Array Destructuring is when we save array items into variables.
// In arrays, order matters and that goes the same for destructuring.
let [player1,player2,player3,player4] = array1;

console.log(player1);
console.log(player4);

let array2 = ["Jokic","Embid","Anthony-Towns","Davis"];

let [center1,,,center2] = array2;

console.log(center1);
console.log(center2);

// Object Destructuring
// Object Destructuring allows us to get the value of a property and save in a variable of the same name.

let pokemon1 = {
	name: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf", "Tackle", "Leech Seed"]
}

// Order does not matter in destructuring Object.
// What matters are the keys/property name.
let {type,level,name,moves,personality} = pokemon1;

console.log(type);
console.log(level);
console.log(moves);
console.log(name);
console.log(personality);//undefined

// Destructuring with a function



function greet(object){

	/*
	When passed user1, object now contains the key-value pairs of user1
	object = {
	name: "Michael",
	position: "Manager",
	income: 90000,
	expenses: 50000
	}
	
	*/

	/*
		If no destructuring:
		console.log(`Hello! ${name}`);
		console.log(`${name} is my friend!`);
		console.log(`Good Luck, ${name}!`);
	*/

	
	/*With destructuring*/
	let {name} = object;//allowed us to get object.name and save it as name.
	// let {position} = object;

	console.log(`Hello! ${name}`);
	console.log(`${name} is my friend!`);
	console.log(`Good Luck, ${name}!`);
	// console.log	(position);
}

greet(user1);

// Mini-Activity:
// Destructure the following object and log the values of the name and price in the console:

let product1 = {
	productName: "Safeguard Handsoap",
	description: "Liquid Handsoap by Safeguard",
	price: 25,
	isActive: true

}


// Display the productName and price in the console by destructuring.
// Send a screenshot of your output in the hangout.

let {productName,description,price,isActive} = product1;

console.log(`Product Name:${productName}`);
console.log(`SRP:${price}`);


/*function displayPriceAndName(product){
	let {productName,price} = product;
	console.log(`Product Name:${productName}`);
	console.log(`SRP:${price}`);
}
displayPriceAndName(product1);

*/

// Arrow Functions
// Arrow Functions are an alternative way of writing function in JS.
// However, there is significant pros and cons between traditionall and arrow function

// traditional function
function displayMsg(){
	console.log(`Hello, World!`);
}

displayMsg();

// Arrow Function
const hello = () => {
	console.log(`Hello, Arrow!`);
}

hello();

// Arrow functions with parameters:

const alertUser = (username) => {
	console.log(`This is an alert for user ${username}`);
}

alertUser("Procorpio1998");

// We don't usuall use let  keyword to assign our arrow function to avoid updating the variable.
/*alertUser = "Hello, sir Procs";

alertUser("procs100");*/

// Arrow and Traditional functions are pretty much the same. They are functions. However, there are some key differences.

// Implicit Return - is the ability of an arrow function to return value without the use of return keyword.

// traditional addNum() function

function addNum(num1,num2){
	let result = num1 + num2;

	return result;
}

let sum1 = addNum(5,10);
console.log(sum1);

// Arrow Function have implicit return. When an arrow function is written in one line, it can return value without return keyword:

const addNumArrow = (num1,num2) => num1+num2;

let sum2 = addNumArrow(10,20);
console.log(sum2);

// Implicit Return will only work on arrow functions written in one line and without {}
// If an arrow function is written in more than one line and with {}, then we will need a return keyword:

/* const subNum = (num1,num2) => {
 	return num1 - num2;
 }*/

const subNum = (num1,num2) => num1 - num2; 

 let difference = subNum(20,10);
 console.log(difference);


// Traditional Functions cs Arrow Functinos as Object Methods

 let character1 = {
 	name: "Cloud Strife",
 	occupation: "SOLDIER",
 	introduceName: function(){
 		// In a traditional function as a method
 		// this refers the object where the method is.
 		console.log(`Hi! I'm ${this.name}.`);
 	},
	introduceJob: () => {
		// In a arrow function as a method
 		// this actually refers to the global window object/the whole document.
 		// console.log(`My job is ${this.occupation}.`);
 		console.log(this);
	} 
 }
 character1.introduceName();
 character1.introduceJob();

/*const sampleObj = {
	name: "Sample1",
	age: 25
}

// sampleObj = "Hello";

sampleObj.name = "Smith";

console.log(sampleObj);*/

// Class Based Object Blueprints
	// In Javascript, Classes are templates of Objects
	// We can use classes to create objects following the structure of the class similar to a constructor function.

 	// Constructor Function
/* function Pokemon(name,type,level){
 	this.name = name;
 	this.type = type;
 	this.level = level;
 }

 let pokemonInstance1 = new Pokemon("Pikachu","Electric",25);
 console.log(pokemonInstance1);
*/
 // With the advent of ES6, we are now introduced to a new way of creating objects with a blueprint with the use of Classes.

 class Car {
 	constructor(brand,model,year){
 		this.brand = brand;
 		this.model = model;
 		this.year = year;
 	}
 }

 let car1 = new Car("Toyota","Vios","2002");
 let car2 = new Car("Cooper","Mini","1967");
 let car3 = new Car("Porsche","911","1968");

 console.log(car1);
 console.log(car2);
 console.log(car3);

 // Mini-Activity
 // Translate the pokemon constructor function into a Class Constructor
 // Creat 2 new pokemons out of the class constructor and save it in their variables.
 // Log the values in the console
 // Share your output in the hangouts.


 class Pokemon {
 	constructor(name,type,level){
 		this.name = name;
 		this.type = type;
 		this.level = level;
 	}
 }

 let pokemonNew1 = new Pokemon("Charizard","Fire",35);
 let pokemonNew2 = new Pokemon("Squirtle","Water",40);
 

 console.log(pokemonNew1);
 console.log(pokemonNew2);
 

// Arrow Function in Array Methods

 let numArr = [2,10,3,10,5];

 // Array Method with Traditional Function
 /*let reduceNumber = numArr.reduce(function(x,y){
 	// Get the sum of all numbers in the array
 	return x+y;
 })*/

// Reduce with Arrow Function
/*let reduceNumber = numArr.reduce((x,y) => {
	return x+y;
})*/

// Reduce with implicit return
let reduceNumber = numArr.reduce((x,y) => x+y);

 console.log(reduceNumber);

// Tip: If you are still getting confused on making arrow functions with array methods, first create the method with a traditional function, then translate that into arrow:

/* let mappedNum = numArr.map(function(num){
 	return num*2;
 })
*/
 let mappedNum = numArr.map((num) => {
 	return num*2;
 })

 console.log(mappedNum);